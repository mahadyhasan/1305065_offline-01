import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;



class Burger{

    String type;
    Burger(String type){
        this.type=type;
    }

    String return_burger(){

        return type;
    }
}



class Extra{

    boolean patty,sauce,cheese;

    boolean get_patty(){
        patty=true;
        return patty;
    }

    boolean get_sauce(){
        sauce=true;
        return sauce;
    }

    boolean get_cheese(){
        cheese=true;
        return cheese;
    }


}


interface Burger_Builder{

   void set_burger();
   void set_patty();
   void set_sauce();
   void set_cheese();

}


class Beef_Burger_Builder implements Burger_Builder{


    int number_of_burger;

    Beef_Burger_Builder(int number_of_burger){

        this.number_of_burger=number_of_burger;
    }

    Extra extra=new Extra();
    Burger burger=new Burger("chicken");

    public void set_burger(){
        burger.return_burger();

    }
    public void set_patty(){
        boolean t=true;
        extra.get_patty();

    }
    public void set_sauce(){
        boolean t=false;
        extra.get_sauce();

    }
    public void set_cheese(){
        boolean t=true;
        
        extra.get_cheese();

    }
}




class Chicken_Burger_Builder implements Burger_Builder{


    int number_of_burger;

    Chicken_Burger_Builder(int number_of_burger){

        this.number_of_burger=number_of_burger;
    }

    Extra extra=new Extra();
    Burger burger=new Burger("beef");

    public void set_burger(){
        burger.return_burger();

    }
    public void set_patty(){
        boolean t=false;
        extra.get_patty();

    }
    public void set_sauce(){
        boolean t=true;
        extra.get_sauce();

    }
    public void set_cheese(){
        boolean t=true;
        extra.get_cheese();

    }
}



class Burger_Order{

    int b,c;


    Burger_Order(int b,int c){
        this.b=b;
        this.c=c;

    }
    Beef_Burger_Builder beef_burger=new Beef_Burger_Builder(b);
    Chicken_Burger_Builder chicken_burger=new Chicken_Burger_Builder(c);

    int Beef_burger(){

        return b;

    }

    int Chicken_burger(){
         return c;

    }


}




class Customers{


    int number_of_people;


    void choose(){

        int beef=0,chicken=0;
        Scanner in = new Scanner(System.in);

        if (number_of_people != 1) {

            System.out.println("You need " + number_of_people + " Burgers total.");
            System.out.println("PLEASE Enter the number of BEEF Burger");
            beef=in.nextInt();
            System.out.println("PLEASE Enter the number of Chicken Burger");
            chicken=in.nextInt();


            if((beef+chicken)!=number_of_people){
                System.out.println("You have ordered more to less coffie");
                System.out.println("Please reinter");
                choose();

            }

            else{

                Burger_Order burger_order=new Burger_Order(beef,chicken);
                int beef_burger=burger_order.Beef_burger();
                int chicken_burger=burger_order.Chicken_burger();
            }

        }

        else if (number_of_people == 1) {

            char type=' ';

            System.out.println("You need One Burger total");
            System.out.println("PLEASE Enter B for BEEF burger");
            System.out.println("OR PLEASE Enter C for Chicken burger");

            BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

            try{
                type=(char)br.read();
            }catch (IOException e){
                System.out.println(e);
            }



            if(type=='B'){
                Burger_Order burger_order=new Burger_Order(1,0);
                int beef_burger=burger_order.Beef_burger();


            }

            else if(type=='C'){

                Burger_Order burger_order=new Burger_Order(0,1);
                int beef_burger=burger_order.Beef_burger();
            }

            else{
                System.out.println("Invalid");
                System.out.println("PLEASE Enter B for BEEF burger");
                System.out.println("OR PLEASE Enter C for Chicken burger");
                choose();
            }

        }


    }






    Customers(int number_of_people){

        this.number_of_people=number_of_people;
        System.out.println("WELCOME TO TAKEOUT BURGER SHOP");
        System.out.println("THERE ARE TWO TYPES OF BURGER IN OUR SHOP: ");
        System.out.println("(i) Beef Burger");
        System.out.println("(ii) Chicken Burger");

        try{
            choose();
        }catch (NullPointerException e){
            System.out.println("Not possible to procede."+e);
        }






    }




}

public class TakeOut {


    public static void main(String[] args) throws IOException {


        char option;
        int number_of_customer=0;

        System.out.println("WELCOME TO TAKEOUT BURGER SHOP");
        System.out.println("DO YOU WANT TO TAKE BURGER ?");
        System.out.println("ENTER CAPITAL-'E'FOR CHECK_IN ");

        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        Scanner scanner=new Scanner(System.in);

        option=(char)br.read();

        while(true) {

            if (option == 'E') {
                System.out.println("ENTER THE NUMBER OF CUSTOMERS");
                number_of_customer =scanner.nextInt();

                Customers customers = new Customers(number_of_customer);


            } else {
                System.out.println("WELCOME TO TAKEOUT BURGER SHOP");
            }

        }

    }
}
